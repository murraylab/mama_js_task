"""

"""

import numpy as np
from itertools import product, combinations
import codecs, json
from sklearn.utils import shuffle


def makeBundles(vals=None,hard=False,df=4):
    """Create bundles from a given set of values"""
    #Default values
    if vals is None:
        vals = np.arange(0,1,.2)
    #normalize to 1
    if np.max(vals) > 1:
        vals = vals/np.max(vals)
    bundles = np.array(list(product(vals, repeat=2)))
    indx = np.arange(0, len(bundles), 1)
    indx_2 = np.array(list(combinations(indx, 2)))
    bundles = np.concatenate((bundles[indx_2[:, 0], :], bundles[indx_2[:, 1], :]), axis=1)
    #Limit bundles to just those which are close to each other
    if hard:
        bundle_val = np.concatenate((np.sum(bundles[:, [0, 2]], axis=1).reshape(len(bundles), 1),
                                     np.sum(bundles[:, [1, 3]], axis=1).reshape(len(bundles), 1)), axis=1)
        I_bund = abs(bundle_val[:, 0] - bundle_val[:, 1]) <= df
        I_att = (abs(bundles[:, 0] - bundles[:, 2]) <= df) * (abs(bundles[:, 1] - bundles[:, 3]) <= df)
        I_neq = abs(bundle_val[:, 0] - bundle_val[:, 1]) > 0.0001
        I = I_bund * I_att * I_neq
        bundles = bundles[I,:]
    np.random.shuffle(bundles)
    return bundles


def quadrantBundles(step=0.05):
    A = np.arange(.5 + step, 1., step)
    B = np.arange(0 + step, .5, step)
    bunds = np.array(list(product(A, B)))
    # Shuffle A and B
    bunds2 = bunds.copy()
    I = np.random.rand(bunds.shape[0]) < .5
    bunds2[I, 0] = bunds[I, 1]
    bunds2[I, 1] = bunds[I, 0]
    # Shuffle side for reference
    I = np.random.rand(bunds.shape[0]) < .5
    bundles = np.ones((bunds.shape[0], 4)) * .5
    bundles[I, 0], bundles[I, 2] = bunds2[I, 0], bunds2[I, 1]
    bundles[I < 1, 1], bundles[I < 1, 3] = bunds2[I < 1, 0], bunds2[I < 1, 1]
    # One last shuffle of order
    I = shuffle(np.arange(0, bundles.shape[0]))
    bundles = bundles[I, :]
    return bundles


def reshapeBundles(bundles,n_atts=2):
    n_alts = int(bundles.shape[1] / n_atts)
    new_bundles = np.zeros((bundles.shape[0], n_alts, n_atts))
    for b in range(bundles.shape[0]):
        for a in range(n_alts):
            new_bundles[b, a, :] = bundles[b, a::n_atts]
    return new_bundles


def saveJson(bundles,file_path='bundles.json'):
    """
    Saves the bundles as a json file
    :param file_path: The path and name of file
    :param bundles: Matrix of bundles
    :return: 0 if no error, 1 if error
    """
    try:
        bundles_list = bundles.tolist()

        json.dump(bundles_list, codecs.open(file_path, 'w', encoding='utf-8'), separators=(',', ':'),indent=4)
        # json.dump(bundles, codecs.open(file_path, 'w', encoding='utf-8'), separators=(',', ':'),
        #       sort_keys=True, indent=4)  ### this saves the array in .json format
        return 0
    except:
        raise ValueError('Bundle save failed.')
        return 1


if __name__ == '__main__':
    #Make with all combinations of bundles
    # vals = np.arange(.3,.9,.2)
    # bundles_o = makeBundles(vals=vals)
    # bundles = reshapeBundles(makeBundles())
    #Subset that are in the relevant quadrant
    bundles = quadrantBundles(step=0.05)
    bundles = reshapeBundles(bundles)

    uncertainty = np.array([0,0.4]) #np.arange(0,.2,.1)
    saveJson(bundles,file_path='bundles.json')
    saveJson(uncertainty, file_path='uncertainty.json')
    print('Bundles made and uncertainty saved!')