/**
 * jspsych-multiAlternative-multiAttribute
 * plugin for choosing between alternatives defined by multiple attributes
 * Warren Woodrich Pettine, March 11 2020
 *
 * documentation: docs.jspsych.org
 */


jsPsych.plugins['multiAlternative-multiAttribute'] = (function() {

    var plugin = {};
    var canvas, ctx, w, h;

    jsPsych.pluginAPI.registerPreload('multiAlternative-multiAttribute', 'stimuli', 'image');

    plugin.info = {
        name: 'free-sort',
        description: '',
        parameters: {
            stimuli: {
                type: jsPsych.plugins.parameterType.STRING,
                pretty_name: 'Stimuli',
                default: undefined,
                array: true,
                description: 'Attribute images to be displayed.'
            },
            choices: {
                type: jsPsych.plugins.parameterType.KEYCODE,
                array: true,
                pretty_name: 'Choices',
                default: jsPsych.ALL_KEYS,
                description: 'The keys the subject is allowed to press to respond to the stimulus.'
            },
            block: {
                type: jsPsych.plugins.parameterType.INT,
                pretty_name: 'Trial block',
                default: 0,
                description: 'Block of trials that this one occured.'
            },
            att_vals: {
                type: jsPsych.plugins.parameterType.INT,
                pretty_name: 'Attribute Values',
                default: undefined,
                array: true,
                description: 'Matrix with an array of attribute values.'
            },
            att_uncertainty: {
                type: jsPsych.plugins.parameterType.INT,
                pretty_name: 'Attribute Uncertainty',
                default: 0,
                description: 'Degree of attribute value uncertainty.'
            },
            basket_colors: {
                type: jsPsych.plugins.parameterType.INT,
                pretty_name: 'Color of baskets',
                default: ['red','blue','green','orange'],
                array: true,
                description: 'Colors that baskets will be presented in.'
            },
            basket_labels: {
                type: jsPsych.plugins.parameterType.INT,
                pretty_name: 'Labels of baskets',
                default: ['A','B','C','D'],
                array: true,
                description: 'labels for baskets.'
            },
            att_size: {
                type: jsPsych.plugins.parameterType.INT,
                pretty_name: 'Attribute Height',
                default: 180,
                description: 'The height of the attributes.'
            },
            offset_x: {
                type: jsPsych.plugins.parameterType.INT,
                pretty_name: 'X offset',
                default: 150,
                description: 'Seperation value in X dimension.'
            },
            offset_y: {
                type: jsPsych.plugins.parameterType.INT,
                pretty_name: 'Y offset',
                default: 30,
                description: 'Seperation value in Y dimension.'
            },
            prompt: {
                type: jsPsych.plugins.parameterType.STRING,
                pretty_name: 'Prompt',
                default: null,
                description: 'It can be used to provide a reminder about the action the subject is supposed to take.'
            },
            canvas_size: {
                type: jsPsych.plugins.parameterType.SELECT,
                pretty_name: 'Canvas size',
                default: '800',
                description: 'Size of the canvas on the screen.'
            },
            button_label: {
                type: jsPsych.plugins.parameterType.STRING,
                pretty_name: 'Button label',
                default:  'Continue',
                description: 'The text that appears on the button to continue to the next trial.'
            },
            trial_duration: {
                type: jsPsych.plugins.parameterType.INT,
                pretty_name: 'Trial duration',
                default: null,
                description: 'How long to show trial before it ends.'
            },
            response_ends_trial: {
                type: jsPsych.plugins.parameterType.BOOL,
                pretty_name: 'Response ends trial',
                default: true,
                description: 'If true, trial will end when subject makes a response.'
            }
        }
    }

    plugin.trial = function(display_element, trial) {

        var html = "";
        // check if there is a prompt and if it is shown above
        if (trial.prompt !== null && trial.prompt_location == "above") {
            html += trial.prompt;
        }

        html += '<div '+
            'id="jspsych-multiAlternative-multiAttribute" '+
            'class="jspsych-multiAlternative-multiAttribute" '+
            'style="position: relative; width:'+trial.sort_area_width+'px; height:'+trial.sort_area_height+'px; border:2px solid #444;"'+
            '></div>';

        display_element.innerHTML = html;

        // Add stimuli to the HTML document
        for (var i = 0; i < trial.stimuli.length; i++) {
            display_element.querySelector("#jspsych-multiAlternative-multiAttribute").innerHTML += '<img '+
                'src="'+trial.stimuli[i]+'" '+
                'data-src="'+trial.stimuli[i]+'" '+
                'class="jspsych-multiAlternative-multiAttribute" '+
                'draggable="false" '+
                'style="display:none;"'+
                '</img>';
        }

        // Add the canvas
        display_element.querySelector("#jspsych-multiAlternative-multiAttribute").innerHTML +=
            '<canvas id="myCanvas"  width="' + trial.canvas_size + '" height="' + trial.canvas_size + '"></canvas>';

        // store response
        var response = {
            rt: null,
            key: null
        };

        // function to end trial when it is time
        var end_trial = function() {

            // kill any remaining setTimeout handlers
            jsPsych.pluginAPI.clearAllTimeouts();

            // kill keyboard listeners
            if (typeof keyboardListener !== 'undefined') {
                jsPsych.pluginAPI.cancelKeyboardResponse(keyboardListener);
            }

            console.log("Trial Stimulus: " + trial.stimuli)
            // gather the data to store for the trial
            var trial_data = {
                "rt": response.rt,
                "stimuli": trial.stimuli,
                "attribute_values": trial.att_vals,
                "uncertainty": trial.att_uncertainty,
                "block": trial.block,
                "key_press": response.key
            };

            // clear the canvas
            (canvas).remove();

            // clear the display
            display_element.innerHTML = '';

            // move on to the next trial
            jsPsych.finishTrial(trial_data);
        };

        // function to handle responses by the subject
        var after_response = function(info) {

            // only record the first response
            if (response.key == null) {
                response = info;
            }

            if (trial.response_ends_trial) {
                end_trial();
            }
        };

        // start the response listener
        if (trial.choices != jsPsych.NO_KEYS) {
            var keyboardListener = jsPsych.pluginAPI.getKeyboardResponse({
                callback_function: after_response,
                valid_responses: trial.choices,
                rt_method: 'performance',
                persist: false,
                allow_held_key: false
            });
        }

        // end trial if trial_duration is set
        if (trial.trial_duration !== null) {
            jsPsych.pluginAPI.setTimeout(function() {
                end_trial();
            }, trial.trial_duration);
        }

        // Create the stimuli for the trial
        var attImgs = display_element.querySelector("#jspsych-multiAlternative-multiAttribute").querySelectorAll("img");

        // called AFTER the page has been loaded
        canvas = display_element.querySelector("#myCanvas");

        // often useful
        w = canvas.width;
        h = canvas.height;

        // important, we will draw with this object
        ctx = canvas.getContext('2d');

        // Get the attributes on the screen
        drawAtts(attImgs,trial.att_vals,trial.att_uncertainty,trial.att_size,trial.offset_x,
            trial.offset_y,trial.basket_colors,trial.basket_labels);

    };

    // helper functions for creating screen

    function drawAtts(attImgs,attVals,uncertainty,attSize,offsetX,offsetY,basketColors,basketLabels){
        console.log('In drawAtts')
        // save the context, use 2D trasnformations
        ctx.save();

        let xStep = w/(attVals.length*1.5)
        x = xStep/2
        var i;
        for (i = 0; i<attVals.length; i++) {
            let y = (h/4);

            let height = (attSize + offsetY) * attVals[i].length;
            let width = offsetX + attSize/2
            ctx.save();

            ctx.translate(x, y)

            ctx.font = "40px Comic Sans MS";
            ctx.fillStyle = basketColors[i];
            ctx.textAlign = "center";
            ctx.fillText("Basket " + basketLabels[i], width/2, -10)

            //Make the basket
            drawLine(0,0,0,height,basketColors[i],2)
            drawLine(0,height,width,height,basketColors[i],2)
            drawLine(width,0,width,height,basketColors[i],2)

            //Include the attributes
            let attHeight = (offsetY/2);
            for (j = 0; j<attVals[i].length; j++) {
                let widthAtt = attSize * ( attImgs[i].width / attImgs[i].height);
                let widthAtt2 = (width-widthAtt)/2;
                drawAtt(attImgs[j],attVals[i][j],uncertainty,attSize,widthAtt2,attHeight)
                attHeight += offsetY + attSize;
            }

            ctx.restore()
            x += xStep
        }

        //Restore the context
        ctx.restore();
    }

    function drawLine(strtX,strtY,endX,endY,color,width){
        ctx.beginPath();
        ctx.moveTo(strtX, strtY);
        ctx.lineTo(endX, endY);
        ctx.lineWidth = width;
        ctx.strokeStyle = color;
        ctx.stroke()
    }

    function drawAtt(attImg,attVal,uncertainty,size,x,y){
        console.log('In drawAtt');
        ctx.save();
        // Calculate the positions and sizes
        let width = size * ( attImg.width / attImg.height);
        let height = size;
        let val = (1-attVal)*height;
        let u = uncertainty*height;
        let uStartY = Math.random() * u + (val-u);
        let uOverX = width*.1;
        // translate the coordinate system, draw relative to it
        ctx.translate(x, y);
        //draw the attribute image
        ctx.drawImage(attImg, 0, 0, width, height);
        //Include box to indicate value
        drawFilledRectangle(0,0, width,val, "black");
        //Include box for uncertainty
        drawFilledRectangle(-uOverX, uStartY, width+uOverX*2,u, "grey");
        ctx.restore();
    }

    function drawFilledRectangle(x, y, width, height, color) {
        //save the context
        ctx.save();

        // translate the coordinate system, draw relative to it
        ctx.translate(x, y);

        ctx.fillStyle = color;
        // (0, 0) is the top left corner of the monster.
        ctx.fillRect(0, 0, width, height);

        // GOOD practice: restore the context
        ctx.restore();
    }

    return plugin;
})();
